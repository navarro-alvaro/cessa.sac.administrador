﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Utilitarios.SACC
{
    public partial class FrmPrincipalMDI : Form
    {
        int id_usuario;
        bool alerta_plataforma, alerta_cajas = false;

        DBMySQL dbMySQL = new DBMySQL();

        public FrmPrincipalMDI(string cargo, int id)
        {
            InitializeComponent();

            switch(cargo)
            {
                case "ADMINISTRADOR":
                    MnuItemUsuarios.Enabled = true;
                    MnuItemAvisos.Enabled = true;
                    MnuItemVideos.Enabled = true;
                    break;
                case "RRPP":
                    MnuItemUsuarios.Enabled = false;
                    MnuItemAvisos.Enabled = true;
                    MnuItemVideos.Enabled = false;
                    break;
                default:
                    MnuItemUsuarios.Enabled = false;
                    MnuItemAvisos.Enabled = false;
                    MnuItemVideos.Enabled = false;
                    break;
            }

            id_usuario = id;

            lblAlertaCajas2.Visible = false;
            lblAlertaPlataforma.Visible = false;
        }

        //Cargar datos de usuarios conectados y tickets en cola
        //Cargar el número de usuarios que se encuentran conectados en Plataforma
        private void UsuariosPlataforma()
        {
            string query =
                "SELECT COUNT(usuarios_log.id) AS 'Usuarios conectados', usuarios.rol AS 'Rol' FROM usuarios_log " +
                "INNER JOIN usuarios " +
                "ON usuarios_log.login = usuarios.login " +
                "WHERE usuarios_log.estado = 'C' AND usuarios.rol = 'PLATAFORMA'" +
                "GROUP BY usuarios.rol; ";

            using (MySqlDataReader dataReader = dbMySQL.Seleccionar(query))
            {
                string CantidadUsuariosPlataforma = "";
                if (dataReader.Read())
                {
                    CantidadUsuariosPlataforma = dataReader.GetString(0);
                    if (CantidadUsuariosPlataforma.Equals("1"))
                    {
                        lblCantUsuariosPlataforma.Text = CantidadUsuariosPlataforma + " Usuario ";
                    }
                    else
                    {
                        lblCantUsuariosPlataforma.Text = CantidadUsuariosPlataforma + " Usuarios";
                    }
                    dataReader.Close();
                }
                else
                {
                    CantidadUsuariosPlataforma = "0";
                    lblCantUsuariosPlataforma.Text = CantidadUsuariosPlataforma + " Usuarios";
                    dataReader.Close();
                }
            }
        }

        //Cargar el número de usuarios que se encuentran conectados en Cajas
        private void UsuariosCajas()
        {
            string query =
                "SELECT COUNT(usuarios_log.id) AS 'Usuarios conectados', usuarios.rol AS 'Rol' FROM usuarios_log " +
                "INNER JOIN usuarios " +
                "ON usuarios_log.login = usuarios.login " +
                "WHERE usuarios_log.estado = 'C' AND usuarios.rol = 'CAJAS'" +
                "GROUP BY usuarios.rol; ";

            using (MySqlDataReader dataReader = dbMySQL.Seleccionar(query))
            {
                string CantidadUsuariosCajas = "";
                if (dataReader.Read())
                {
                    CantidadUsuariosCajas = dataReader.GetString(0);
                    if (CantidadUsuariosCajas.Equals("1"))
                    {
                        lblCantUsuariosCajas.Text = CantidadUsuariosCajas + " Usuario ";
                    }
                    else
                    {
                        lblCantUsuariosCajas.Text = CantidadUsuariosCajas + " Usuarios";
                    }
                    dataReader.Close();
                }
                else
                {
                    CantidadUsuariosCajas = "0";
                    lblCantUsuariosCajas.Text = CantidadUsuariosCajas + " Usuarios";
                    dataReader.Close();
                }
            }
        }

        // Cargar el número de tickets en Plataforma que se encuentran en cola (estado < 2)
        private void TicketsPlataforma()
        {
            string query = 
                "SELECT COUNT(id) AS 'Tickets pendientes' " +
                "FROM tickets " +
                "WHERE fecha_solicitud = CURRENT_DATE " +
                "AND tipo IN ('A', 'B', 'C') AND " +
                "estado < 2 " +
                "ORDER BY id DESC;";
            using (MySqlDataReader dataReader = dbMySQL.Seleccionar(query))
            {
                string CantidadTicketsPlataforma = "";
                if (dataReader.Read())
                {
                    CantidadTicketsPlataforma = dataReader.GetString(0);
                    lblCantTicketPlataforma.Text = CantidadTicketsPlataforma + " Tickets";
                    dataReader.Close();
                }
            }
        }

        // Cargar el número de tickets en Cajas que se encuentran en cola (estado < 2)
        private void TicketsCajas()
        {
            string query =
                "SELECT COUNT(id) AS 'Tickets pendientes' " +
                "FROM tickets " +
                "WHERE fecha_solicitud = CURRENT_DATE " +
                "AND tipo IN ('G', 'H', 'I') AND " +
                "estado < 2 " +
                "ORDER BY id DESC;";
            using (MySqlDataReader dataReader = dbMySQL.Seleccionar(query))
            {
                string CantidadTicketsCajas = "";
                if (dataReader.Read())
                {
                    CantidadTicketsCajas = dataReader.GetString(0);
                    lblCantTicketCajas.Text = CantidadTicketsCajas + " Tickets";
                    dataReader.Close();
                }
            }
        }

        private void Alertas()
        {
            int cantUsuariosPlat = Convert.ToInt32(lblCantUsuariosPlataforma.Text.Substring(0, 1));
            int cantUsuariosCajas = Convert.ToInt32(lblCantUsuariosCajas.Text.Substring(0, 1));
            int cantTicketsPlat = 0;
            int cantTicketsCajas = 0;
            if (lblCantTicketPlataforma.Text.Length > 9)
            {
                cantTicketsPlat = Convert.ToInt32(lblCantTicketPlataforma.Text.Substring(0, 2));
            }
            else
            {
                cantTicketsPlat = Convert.ToInt32(lblCantTicketPlataforma.Text.Substring(0, 1));
            }

            if (lblCantTicketCajas.Text.Length > 9)
            {
                cantTicketsCajas = Convert.ToInt32(lblCantTicketCajas.Text.Substring(0, 2));
            }
            else
            {
                cantTicketsCajas = Convert.ToInt32(lblCantTicketCajas.Text.Substring(0, 1));
            }

            if (cantUsuariosPlat != 0)
            {
                if ((cantTicketsPlat / cantUsuariosPlat) >= 5)
                {
                    alerta_plataforma = true;
                }
                else
                {
                    alerta_plataforma = false;
                }
            }
            if (cantUsuariosCajas != 0)
            { 
                if ((cantTicketsCajas / cantUsuariosCajas) >= 5)
                {
                    alerta_cajas = true;
                }
                else
                {
                    alerta_cajas = false;
                }
            }

            if (alerta_cajas == false && alerta_plataforma == false)
            {
                pnlInformativo.Height = 155;
                lblAlertaPlataforma.Visible = false;
                lblAlertaCajas1.Visible = false;
                lblAlertaCajas2.Visible = false;
            }
            else
            {
                if ((alerta_cajas == true && alerta_plataforma == false) || (alerta_cajas = false && alerta_plataforma == true))
                {
                    pnlInformativo.Height = 200;
                    lblAlertaPlataforma.Visible = false;
                    lblAlertaCajas1.Visible = false;
                    lblAlertaCajas2.Visible = false;
                    if (alerta_cajas == true && alerta_plataforma == false)
                    {
                        lblAlertaCajas1.Visible = true;
                    }
                    else
                    {
                        if (alerta_cajas = false && alerta_plataforma == true)
                        {
                            lblAlertaPlataforma.Visible = true;
                        }
                    }
                }
                else
                {
                    if (alerta_cajas == true && alerta_plataforma == true)
                    {
                        pnlInformativo.Height = 245;
                        lblAlertaPlataforma.Visible = true;
                        lblAlertaCajas1.Visible = false;
                        lblAlertaCajas2.Visible = true;
                    }
                }
            }
        }

        //Funcionalidad de los elementos en menú
        private void EMnuItemSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MnuItemHerramientas_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = MnuItemHerramientas.Checked;
        }

        private void MnuItemEstado_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = MnuItemEstado.Checked;
        }

        private void MnuItemCascada_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void MnuItemVertical_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void MnuItemHorizontal_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void MnuItemCerrarTodo_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void MnuItemUsuarios_Click(object sender, EventArgs e)
        {
            FrmUsuarios frm = new FrmUsuarios(id_usuario);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemAvisos_Click(object sender, EventArgs e)
        {
            FrmAvisos frm = new FrmAvisos(id_usuario);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemVideos_Click(object sender, EventArgs e)
        {
            FrmListadoVideos frm = new FrmListadoVideos();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemReportesAtendidos_Click(object sender, EventArgs e)
        {
            FrmPreReporteUsuario frm = new FrmPreReporteUsuario();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemConfCorreo_Click(object sender, EventArgs e)
        {
            FrmConfCorreo frm = new FrmConfCorreo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemLogAccesos_Click(object sender, EventArgs e)
        {
            FrmLogUsuarios frm = new FrmLogUsuarios();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemComparativaAtencion_Click(object sender, EventArgs e)
        {
            FrmReporteComparativa frm = new FrmReporteComparativa();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemConfAtencion_Click(object sender, EventArgs e)
        {
            FrmConfAtencion frm = new FrmConfAtencion();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemAtencionPlataforma_Click(object sender, EventArgs e)
        {
            FrmReporteAtencionPlataforma frm = new FrmReporteAtencionPlataforma();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MnuItemAtencionCajas_Click(object sender, EventArgs e)
        {
            FrmReporteAtencionCajas frm = new FrmReporteAtencionCajas();
            frm.MdiParent = this;
            frm.Show();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            //CargarUsuarios();
            UsuariosPlataforma();
            UsuariosCajas();
            TicketsPlataforma();
            TicketsCajas();
            Alertas();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Alertas();
        }

        private void MnuItemHorasPico_Click(object sender, EventArgs e)
        {
            FrmReporteHorasPico frm = new FrmReporteHorasPico();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
